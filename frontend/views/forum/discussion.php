<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ForumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Forum Tanya Jawab');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forum-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if (!Yii::$app->user->isGuest) echo Html::a(Yii::t('app', 'New Thread'), ['create-discussion'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item', 'style' => 'background:white; border-radius:4px; padding:10px; margin:10px 0'],
        'itemView' => function ($model, $key, $index, $widget) {
            return '<big>'.Html::a(Html::encode($model->name), ['view', 'id' => $model->id]).'</big>'
            .'<br><small>'.Yii::$app->formatter->asDatetime($model->created_at).' - '.$model->createdBy->username.'</small>';
        },
    ]) ?>
</div>
