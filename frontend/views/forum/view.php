<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Forum */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forum-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box box-primary">
        <div class="box-body">
            <div class="article">
                <p><i><?= Yii::$app->formatter->asDatetime($model->created_at).' - '.$model->createdBy->username; ?></i></p>
                <?= nl2br(Yii::$app->formatter->asHtml($model->content)); ?>
            </div>

            <div class="discussion" style="margin-top: 20px; padding-top: 10px; border-top: 1px dashed #ddd">
                <div class="replies">
                    <?php $dataProvider = new \yii\data\ArrayDataProvider([
                        'allModels' => $model->forumReplies,
                        'pagination' => false,
                    ]); ?>

                    <?php if ($model->forumReplies) echo ListView::widget([
                        'dataProvider' => $dataProvider,
                        'summary' => false,
                        'itemOptions' => ['class' => 'item', 'style' => 'background:#fafafa; border-radius:4px; padding:10px; margin:10px 0'],
                        'itemView' => function ($model, $key, $index, $widget) {
                            return '<i class="small"><b>'.Yii::$app->formatter->asDatetime($model->created_at).' - '.$model->createdBy->username.' : </b></i><br>'.
                            nl2br(Html::encode($model->content));
                        },
                    ]) ?>
                </div>
                <?php if (!Yii::$app->user->isGuest) : ?>
                <div class="reply-form">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($modelReply, 'content')->textarea(['rows' => 3])->label('Add Reply :') ?>
                    <div class="form-group">
                        <?= Html::submitButton('<i class="fa fa-submit"></i> Send', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>
