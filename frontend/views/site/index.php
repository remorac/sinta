<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>

        <p class="lead">Selamat datang di Komunitas Lovebird Indonesia!</p>

        <!-- <p><a class="btn btn-lg btn-success" href="#">Bergabung</a></p> -->
    </div>

    <div class="body-content">

        <div class="row text-center">
            <div class="col-lg-4">
                <div style=" background: white; padding:20px; border-radius: 10px; box-shadow: 0 0 1px rgba(0,0,0,0.1); opacity: 0.9; margin-bottom: 20px">
                <h2>Pusat Informasi</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <!-- <p><a class="btn btn-default" href="#">Yii Documentation &raquo;</a></p> -->
                </div>
            </div>
            <div class="col-lg-4">
                <div style=" background: white; padding:20px; border-radius: 10px; box-shadow: 0 0 1px rgba(0,0,0,0.1); opacity: 0.9; margin-bottom: 20px">
                <h2>Tanya Jawab</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <!-- <p><a class="btn btn-default" href="#">Yii Forum &raquo;</a></p> -->
                </div>
            </div>
            <div class="col-lg-4">
                <div style=" background: white; padding:20px; border-radius: 10px; box-shadow: 0 0 1px rgba(0,0,0,0.1); opacity: 0.9; margin-bottom: 20px">
                <h2>Jual Beli</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <!-- <p><a class="btn btn-default" href="#">Yii Extensions &raquo;</a></p> -->
                </div>
            </div>
        </div>

    </div>
</div>
