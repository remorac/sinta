<?php

namespace frontend\controllers;

use Yii;
use backend\models\Forum;
use backend\models\ForumSearch;
use backend\models\ForumReply;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ForumController implements the CRUD actions for Forum model.
 */
class ForumController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Forum models.
     * @return mixed
     */
    public function actionDiscussion()
    {
        $searchModel = new ForumSearch();
        $queryParams = Yii::$app->request->queryParams;        

        if (!isset($queryParams['ForumSearch'])) $queryParams['ForumSearch'] = array();
        $queryParams['ForumSearch'] = array_merge($queryParams['ForumSearch'], ['is_trading' => 0]);
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('discussion', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTrading()
    {
        $searchModel = new ForumSearch();
        $queryParams = Yii::$app->request->queryParams;        

        if (!isset($queryParams['ForumSearch'])) $queryParams['ForumSearch'] = array();
        $queryParams['ForumSearch'] = array_merge($queryParams['ForumSearch'], ['is_trading' => 1]);
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('trading', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Forum model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $modelReply = new ForumReply;
        $modelReply->forum_id = $id;

        if ($modelReply->load(Yii::$app->request->post())) {
            if ($modelReply->save()) {
                $modelReply = new ForumReply;
                $modelReply->forum_id = $id;
            }
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelReply' => $modelReply,
        ]);
    }

    /**
     * Creates a new Forum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateDiscussion()
    {
        $model = new Forum();
        $model->is_trading = 0;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create-discussion', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Forum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTrading()
    {
        $model = new Forum();
        $model->is_trading = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create-trading', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Forum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing Forum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the Forum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Forum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Forum::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
