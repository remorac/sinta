<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

Yii::$app->params['showTitle'] = false;
$this->title = 'SINTA';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>SINTA</h1>

        <p class="lead">Sistem Input Tamu Adipala</p>

        <!-- <p><a class="btn btn-lg btn-success" href="#">Bergabung</a></p> -->
    </div>

    <div class="body-content">

        <div class="row text-center">
            <div class="col-lg-4 col-lg-offset-4">
                <div style=" background: white; padding:20px; border-radius: 10px; box-shadow: 0 0 1px rgba(0,0,0,0.1); opacity: 0.9; margin-bottom: 20px">
                <h2>Input Data Tamu</h2>
                <p>Klik tombol dibawah untuk input data tamu.</p>
                <p><a class="btn btn-default" href="<?= Url::to(['guest/create']) ?>">Form Input &raquo;</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
