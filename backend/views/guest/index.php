<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\DatePicker;
use backend\models\Guest;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GuestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guests';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="alert alert-info" id="notification" style="display: none"></div>

<div class="guest-index box box-primary box-body">

    <?php 
        $exportColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            'id',
            'nama',
            'ktp',
            'alamat',
            'telp',
            'nopol',
            'asal_perusahaan',
            'tujuan',
            'keperluan',
            'zona_area',
            'keterangan',
            'pengikut',
            'tanggal:date',
            'status',
        ];

        $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $exportColumns,
            'filename' => 'Guests',
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default'
            ],
            'target' => ExportMenu::TARGET_SELF,
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'styleOptions' => [
                ExportMenu::FORMAT_EXCEL_X => [
                    'font' => [
                        'color' => ['argb' => '00000000'],
                    ],
                    'fill' => [
                        'type' => PHPExcel_Style_Fill::FILL_NONE,
                        'color' => ['argb' => 'DDDDDDDD'],
                    ],
                ],
            ],
            'pjaxContainerId' => 'grid',
        ]);

        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right serial-column'],
                'contentOptions' => ['class' => 'text-right serial-column'],
            ],
            [
                'contentOptions' => ['class' => 'action-column nowrap text-left'],
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open btn btn-xs btn-default btn-text-info']);
                    },
                    'update' => function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-pencil btn btn-xs btn-default btn-text-warning']);
                    },
                    'delete' => function ($url) {
                        return Html::a('', $url, [
                            'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger', 
                            'data-method' => 'post', 
                            'data-confirm' => 'Are you sure you want to delete this item?']);
                    },
                ],
            ],
            [
                'attribute' => 'status',
                'filter' => Guest::$status,
            ],
            // 'id',
            'nama',
            'ktp',
            'alamat',
            'telp',
            'nopol',
            'asal_perusahaan',
            'tujuan',
            'keperluan',
            [
                'attribute' => 'zona_area',
                'filter' => Guest::$zona,
            ],
            [
                'attribute' => 'keterangan',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'pengikut',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            [
                'attribute' => 'tanggal',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            'created_at:datetime:Jam Datang',
        ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'pjax' => true,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'toolbar'=> [
            // Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
            Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            '{toggleData}',
            // $exportMenu,
        ],
        'panel' => [
            'type' => 'no-border',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-6">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-6">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
        'pjaxSettings' => ['options' => ['id' => 'grid']],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>

</div>

<?php
    $this->registerJs(' 
        notify();
        ', \yii\web\VIEW::POS_READY
    ); 

    $this->registerJs('
        var counter = '.Guest::find()->count('id').';
        var old_diff= 0;
        var diff    = 0;

        function notify() {

            function checkNewRecord() {
                console.log(counter);
                $.ajax({
                    url: "'.Url::to(['guest/summary']).'",
                    data: "",
                    type: "get",
                    cache: false,
                    success:function(r) {
                        if (r > counter) {
                            diff = r - counter;
                            if (diff != old_diff) { $("div#notification").hide(); old_diff = diff}
                            $("div#notification").html("<big><b>"+diff+"</b> tamu baru datang.</big><br><small>Tekan tombol Reload untuk data lebih detail</small>");
                            $("div#notification").fadeIn();
                        }
                    }
                });
            }

            checkNewRecord();
            var timeinterval = setInterval(checkNewRecord, 5000);
        }
        ', \yii\web\VIEW::POS_END
    ); 
?>