<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Guest */

$this->title = 'New Guest';
// $this->params['breadcrumbs'][] = ['label' => 'Guests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
