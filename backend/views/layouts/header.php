<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
	<nav class="navbar navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<a href="<?= Url::base() ?>" class="navbar-brand"><b>SINTA</b></a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<i class="fa fa-bars"></i>
				</button>
			</div>

			<?php if (!Yii::$app->user->isGuest) : ?>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<li><?= Html::a('Data Tamu', ['guest/index']) ?></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
			<?php endif; ?>

			<!-- Navbar Right Menu -->
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<?php if (Yii::$app->user->isGuest) { ?>
					<li class="">
						<a href="<?= Url::to(['site/login']) ?>">Login</a>
					</li>
					<?php } else { ?>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::$app->user->identity->username ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li class="" style="text-align:right;">
	                            <?= Html::a(
	                                'Ganti Password',
	                                ['/site/change-password'],
	                                ['style' => 'padding:10px; color:#444']
	                            ) ?>
	                        </li>
	                        <li class="" style="text-align:right;">
	                            <?= Html::a(
	                                'Sign out',
	                                ['/site/logout'],
	                                ['style' => 'padding:10px; color:#444', 'data-method' => 'post']
	                            ) ?>
	                        </li>
						</ul>
					</li>

					<?php } ?>

				</ul>
			</div>
			<!-- /.navbar-custom-menu -->
		</div>
		<!-- /.container-fluid -->
	</nav>
</header>
