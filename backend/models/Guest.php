<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "guest".
 *
 * @property integer $id
 * @property string $nama
 * @property string $ktp
 * @property string $alamat
 * @property string $telp
 * @property string $nopol
 * @property string $asal_perusahaan
 * @property string $tujuan
 * @property string $keperluan
 * @property string $zona_area
 * @property string $keterangan
 * @property string $pengikut
 * @property string $tanggal
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Guest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guest';
    }

    public static $status = [
        'Datang' => 'Datang',
        'Bertemu' => 'Bertemu',
        'Pulang' => 'Pulang',
    ];

    public static $zona = [
        'Hijau' => 'Hijau',
        'Kuning' => 'Kuning',
        'Merah' => 'Merah',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'tujuan', 'keperluan', 'tanggal'], 'required'],
            [['keterangan', 'pengikut'], 'string'],
            [['tanggal'], 'safe'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['nama', 'ktp', 'alamat', 'telp', 'nopol', 'asal_perusahaan', 'tujuan', 'keperluan', 'zona_area', 'status'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'ktp' => 'No. KTP',
            'alamat' => 'Alamat',
            'telp' => 'Telp',
            'nopol' => 'No. Polisi Kendaraan',
            'asal_perusahaan' => 'Asal Perusahaan',
            'tujuan' => 'Bertemu dengan',
            'keperluan' => 'Keperluan',
            'zona_area' => 'Zona Area',
            'keterangan' => 'Keterangan',
            'pengikut' => 'Pengikut',
            'tanggal' => 'Tanggal',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
