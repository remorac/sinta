<?php 

return [
	'Create' => 'Tambah',
	'Update' => 'Ubah',
	'Delete' => 'Hapus',

	'Create Allocation' => 'Tambah Alokasi',
	'Update Allocation' => 'Ubah Alokasi',

	'Create Event' => 'Tambah Event',
	'Update Event' => 'Ubah Event',

	'Create Expense' => 'Tambah Pengeluaran',
	'Update Expense' => 'Ubah Pengeluaran',

	'Create Profit Taking' => 'Tambah Penyaluran Keuntungan',
	'Update Profit Taking' => 'Ubah Penyaluran Keuntungan',

	'Are you sure you want to delete this item?' => 'Anda yakin untuk menghapus data ini?', 

	'Name' => 'Nama',

	'Allocation' => 'Alokasi',
	'Allocations' => 'Alokasi',
	'Percentage' => 'Persentase',
	
	'Customer' => 'Pelanggan',
	'Customers' => 'Pelanggan',
	'Phone' => 'Telepon',
	'Address' => 'Alamat',
	'Province' => 'Provinsi',
	'District' => 'Kabupaten/Kota',
	'Subdistrict' => 'Kecamatan',
	'Village' => 'Desa/Kelurahan',

	'Expense' => 'Pengeluaran',
	'Expenses' => 'Pengeluaran',
	'Date' => 'Tanggal',
	'Amount' => 'Jumlah',

	'Expense Category' => 'Jenis Pengeluaran',
	'Expense Categories' => 'Jenis Pengeluaran',

	'Events' => 'Event',
	'Price' => 'Harga',
	'Paid' => 'Jumlah Telah Dibayar',
	'Pax' => 'Jumlah Porsi',
	'Coupon' => 'Nomor Kupon',
	'Is Done' => 'Status',
	'Description' => 'Keterangan',


	'Profit Taking' => 'Penyaluran Keuntungan',
	'Profit Takings' => 'Penyaluran Keuntungan',
]; 
