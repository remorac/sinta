<?php

namespace backend\helpers;

use yii\helpers\Url;

class ReportHelper
{
	public static function months() {
		return [
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember',
		];
	}

	public static function header($title) {
		return '
			<table width="100%" style="margin-bottom:20px">
				<tr>
					<td align="left" valign="center" width="15%"><img src="img/logo.png" width="50%"></td>
					<td align="center" valign="center">
						<font size="4">JNE</font>				
						<br><font size="5">KANTOR WILAYAH PADANG</font>
						<br>
						<br>'.$title.'
					</td>
					<td width="15%">&nbsp;</td>
				</tr>
			</table>
		';
	}

	public static function footer() {
		return '
			<center>Padang, '.date('d F Y').'</center>
			<table width="100%" style="margin-bottom:20px">
				<tr>
					<td width="33%" align="center" >						
						<br>Dibuat oleh
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>____________________________________
						<br>Petugas
					</td>
					<td width="34%" align="center">						
						
					</td>
					<td width="33%" align="center">
						<br>Diketahui oleh
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>____________________________________
						<br>Kepala
					</td>
				</tr>
			</table>
		';
	}
}
